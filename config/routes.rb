Rails.application.routes.draw do
  get 'loyalty_api/index'
  post 'loyalty_api/create'

  get 'loyalty_api/show'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
