# README


* Rails version: 5.2.3

* Ruby version: ruby 2.5.3p105 (2018-10-18 revision 65156) [x64-mingw32]

* Database creation: Default Sqlite database. A database file also included.

* Database initialization: Run migrations. `rails db:migrate`

* Deployment instructions:

1) Install the rails, and it is a defualt file strucuture.
2) Run `rails s` to start the server.
3) Go to: http://localhost:3000/loyalty_api/index

* New Files added:

1) /app/controllers: loyalty_api_controller  #Contains all the logical structure
2) /app/views:

    a) create.html.erb
    b) index.html.erb
    
3) /app/models

    a) invite_user
    b) invite
    c) user
    
4) db

    a) development.sqlite3
    b) 3 Migrations
    
5) Update Routes

* Routes

There are three routes: (You can edit with your host here instead of default localhost:3000)

1) (GET) http://localhost:3000/loyalty_api/index  #To enter the input and calculate the loyalty points .
2) (POST) http://localhost:3000/loyalty_api/create #To post the data, and all the logic contains in this action and path.
3) (GET) http://localhost:3000/loyalty_api/show #To get the information as stated in the PDF and it is a JSON response.

* Database (Below are the pictures)

1.  There are three tables: Users, Invites, InviteUser
2.  User: which contains the user system, and loyalty points
3.  Invites: keeps track of Invitiations, just like Invitiation Management
4.  InviteUser: Mimics the Joint Tables, just added for mimicing.

* Images

1.  User Table: https://drive.google.com/open?id=1jL1XecXubSIW9PWwBq1pR6LzQo0Y3ewB
2.  Invite Table: https://drive.google.com/open?id=1Yc9rtV74Qe7oRQqY3jsmcpzQaNrsx0Sb
3.  InviteUser Table: https://drive.google.com/open?id=1EHdzRCBXYsqtg79mCbOQzZUxioqBc7B4


#Briefly document the decisions you’ve taken in the process and why you’ve chosen so.

* Decisions Taken:
1.  Which framework needs to choose? I have filtered two Hanami and Rails. Then I chose Rails as I worked on it earlier.
2.  The logic and database architecture. I had to change a few times the architecture of the database.

1.  I have chosen Ruby on Rails as will be one of the stack which I will be working on if you give me the offer.
2.  As the document stated Ruby as the language, therefore I thought to take this application to the next level, by integrating real-world system, which looks good for both of us, and ease of experience.

* The Logic:
1.  I have created three tables as stated above. It also mimics the associations and joins. 
2.  The user table contains various columns, but the main are id (pk), username (unique, index), loyalty points and invited by user information.
3.  The invited table contains the invitation management, and contains columns such as id (pk), invited by, invited username (index, unique), users id (index). It tracks the invitiation management and ensures uniqueness to each invitation sent.
4.  The inviteuser table just mimics the joins, and do not have any real use in this application.
5.  /loyalty_api/index: This is a root page where, one can input the data to process on, layout controller is the main controller, it then posts to /create action in the same controller.
6.  /loyalty_api/create action in the controller, have complex logic, which is given below.
7.  /loyalty_api/shows action gives you the Json format which is asked in PDF document. It just takes data from User table, extract two components, one is username and other is the loyalty points and converts to hash and Jsonify the array and sends to the render.

* Create Action 

1.  It takes params from text area from the form which is placed in index view template
2.  As the input is not a JSON, or XML or easily processable data, therefore I converted the whole input to array of strings.
3.  I first removes "\n" and "\r" from the string, then converts to the array.
4.  The array of input string, then sent to a helper function.
5.  The helper function then segregate each string into individual components, which extracts the date and time, invitee, recommends/accepts phrase and the invited.
6.  Another helper function checks the date and time, if validated it further process.
7.  The data, then is segregated into "recommends" and "accepts"
8.  If recommends, then it creates the invited user in the user table, and creates an invitation in invite table and sets various information in the database.
9.  If accepts, then invite table is updated and a helper function calculates the "levels" and loyalty points and then it sets the loyalty points in the users table.
10.  Please check "Loyalty Api Controller " for the full code.

* Inputs


* It need to have three or four parts:

```
1) For recommends: [YYY-MM-DD] [HH:MM] [INVITEE] recommends [INVITED]
2) For accepts: [YYY-MM-DD] [HH:MM] [INVITED] accepts
3) There can be no "\n or \r" at the starting of the input, but later there are no restrictions.
```

* This apps mimics like event system, which on events the logic runs.


One can send input with the following format:

```
2018-06-12 09:41 M recommends N
2018-06-14 09:41 N accepts
2018-06-16 09:41 N recommends C
2018-06-17 09:41 C accepts
2018-06-19 09:41 C recommends Q
2018-06-23 09:41 Q recommends D
2018-06-25 09:41 D accepts

```

or

```
2018-06-12 09:41 A recommends B

2018-06-14 09:41 B accepts

2018-06-16 09:41 B recommends C

2018-06-17 09:41 C accepts

2018-06-19 09:41 C recommends D

2018-06-23 09:41 B recommends D

2018-06-25 09:41 D accepts

```

or

```
2018-06-12 09:41 A recommends B
2018-06-14 09:41 B accepts
```

or

```
2018-06-12 09:41 A recommends B


2018-06-14 09:41 B accepts
```

or

```
2018-06-12 09:41 A recommends B


2018-06-12 09:41 F recommends D
```

or

```
2018-06-12 09:41 A recommends B

```

or

```
2018-06-12 09:41 F accepts

```

or any other inputs in the structure given above.


# DONT (No space in \n or \r in starting)
```




2018-06-12 09:41 A recommends B


2018-06-12 09:41 F recommends D
```