class LoyaltyApiController < ApplicationController
  skip_before_action :verify_authenticity_token, :only => [:create]

  def index
    @user_for_ui = User.all
  end

  def show
    @user_for_json = User.all
    @array_for_json_response = [] #For creating the JSON array

    #Extracting data from the User information according to the need in PDF
    for pos in 0...@user_for_json.length
      hash_construct = {@user_for_json[pos].username.to_s => @user_for_json[pos].loyalty_points.to_f}
      @array_for_json_response.push(hash_construct)
    end   
    render json: @array_for_json_response #Sending JSON response
  end

  def create
    ############################################# Params from Text Area and Operations on the string ############################################   
    @loyalty_input_string = params[:loyalty_test_input] #param from Form Text Area
    @user_data_array = [] #To store the input array
    @start_index = 0 #For start reference in further loop creating seperate arrays
    @first_r_encountered = 0 #for first \r check point which comes from Text area as the text input may contain more than one r
    @array_for_index = [] #array for keeping (start,end) points [[start, end], [start,end],.......] to iterate in @lyalty_input_string

    #for loop to create separate array for indexes so that we can seperate input lines
    for pos in 0...@loyalty_input_string.length
      if @loyalty_input_string[pos] == "\r" && @first_r_encountered == 0 && @loyalty_input_string[pos] != "\n"
        @array_for_index.push([@start_index,(pos)]) #push the start_index and current position in the array
        @start_index = pos+1 
        first_r_encountered = 1 #flagging that this is the first \r and further \r s will not be pushed in the array
      elsif @loyalty_input_string[pos] == "\n" && @loyalty_input_string[pos+1] != "\r" && @loyalty_input_string[pos+1] != "\n"
        #if it is a \n then skip and dont push it in the index array
        @start_index = pos + 1 
        @first_r_encountered = 0
      elsif pos == (@loyalty_input_string.length-1)
        @array_for_index.push([@start_index,(@loyalty_input_string.length)]) #push the start_index and current position in the array
      end
    end
    @temp_string = "" #temp container for string   
    for key in 0...@array_for_index.length
      @temp_string="" #re-assigning the empty string when work is complete so that old values don't push it in the array
        for data_string_char in @array_for_index[key][0]...@array_for_index[key][1]
          @temp_string = @temp_string + @loyalty_input_string[data_string_char]
        end
        if @temp_string != "\n" #A saftey valve to keep away \n values from final array.
          @user_data_array.push(@temp_string)
        end
    end

    #Initialising "A" user as a start if not already.  
    result = User.exists?(username: "A")
    if result == false
      User.create(username: "A", date_from_input: "2016-11-13 16:02", loyalty_points: 0, invited_by: "none")
    end
    
    #Now sending and operating on invites.
    x = 1
    @user_data_array.each do |key|
      save_invites_operations(key, x)
      x += 1
    end
    ############################################# Text Area operation Ends ############################################

    @user_for_ui = User.all # For the information on Create page.
  end

  #Helper method for saving into the database
  def save_invites_operations(data_to_operate, idx)
    #idx is the nth array reference for error

    #splitting the string from array input
    data_to_operate = data_to_operate.gsub(/\s+/m, ' ').gsub(/^\s+|\s+$/m, '').split(" ") #spliting the string into different data 

    #time validation check, if fails then this can show error, and invites will not be sent
    date_time_input_validation = validate_date_time(data_to_operate[0], data_to_operate[1])
    if date_time_input_validation == false
      return "Date and Time validation failed in " + idx +" position of input"
    end

    #Assuming for the test that the data given is as represented in the PDF document 
    begin
      if data_to_operate[3] == "recommends" || data_to_operate[3] == "recommend"
        #Extrating Data
        user_which_is_inviting = data_to_operate[2]
        user_inviting_to = data_to_operate[4]
        user_date_time_stamp_from_input = data_to_operate[0] + " " + data_to_operate[1]
        accepted_or_not = 0

        #What if the invitng user doesn't exsits just Like A?
        @user_which_is_inviting_exists_or_not = User.exists?(username: user_which_is_inviting)
        if @user_which_is_inviting_exists_or_not == false
          User.create(username: user_which_is_inviting, date_from_input: "2016-11-13 16:02", loyalty_points: 0, invited_by: "none")
        end

        #finding the username if already, and checking the invitiation is sent already or not.
        @user = User.find_by username: user_which_is_inviting 

        result = Invite.exists?(invited_to_username: user_inviting_to)

        inivted_user_search = User.exists?(username: user_inviting_to)
        if inivted_user_search == false
          User.create!(username: user_inviting_to, date_from_input: user_date_time_stamp_from_input, loyalty_points: 0, invited_by: user_which_is_inviting)
        end  
        #Finding the Invite from the current user and to the new user, if it exists it will not create and if it is not then it will create
        if result == false
          invite_creation = Invite.create(users_id: @user.id, invited_to_username: user_inviting_to, accepted_or_not: 0, date_from_input: user_date_time_stamp_from_input, invited_by: user_which_is_inviting)
          InviteUser.create(users_id: @user.id, invites_id: invite_creation.id)      
        end
        
      elsif data_to_operate[3] == "accepts" || data_to_operate[3] == "accept"
        user_which_is_accepting = data_to_operate[2]
        user_date_time_stamp_from_input = data_to_operate[0] + " " + data_to_operate[1]
        @user = User.find_by username: user_which_is_accepting
        
        #Finding the user which invited this person
        @user_invited_by = @user.invited_by
        @user_invited_by = User.find_by username: @user_invited_by
        
        @invite_to_update = Invite.new #Just initialising for probable "Nil" class error , not required here 
        @invite_to_update = Invite.find_by invited_to_username: user_which_is_accepting
        if @invite_to_update.accepted_or_not == 0
          @invite_to_update.update(date_from_input: user_date_time_stamp_from_input, accepted_or_not: 1)
          key_of_user = 0; #To determine the Key
          invited_by_user = @user_invited_by       
          loop do  
            user_to_send_in_the_tree = User.find_by username: invited_by_user.username
            calculate_loyalty_points_and_save( user_to_send_in_the_tree.id, user_to_send_in_the_tree.username, key_of_user)
            invited_by_user = User.find_by username: user_to_send_in_the_tree.invited_by 
            key_of_user = key_of_user + 1
            if(user_to_send_in_the_tree.invited_by == "none")
            break
            end
          end
        end
      end        
    rescue StandardError => e  
      puts e.message  
      puts e.backtrace.inspect  
    end 
  end

  #Helper Method for Checking the Date and Time in the Array at position 0 and 1 in the splited string
  def validate_date_time(date_str, time_str)
    date_time_string = date_str + " " + time_str
    valid_formats = ["%Y-%m-%d %I:%M"] 
    #see http://www.ruby-doc.org/core-1.9.3/Time.html#method-i-strftime for more 
    valid_formats.each do |format|
      valid = Time.strptime(date_time_string, format) rescue false
      return true if valid
    end 
    return false
  end

  #Helper method for updating loyalty points
  def calculate_loyalty_points_and_save(user_id, user_username,k)
    @loyalty_points = ((0.5) ** k)
    if k == 0
      @loyalty_points = 1
    end
    user = User.find(user_id)
    user_loyalty_points = user.loyalty_points
    user_loyalty_points = user_loyalty_points + @loyalty_points
    user.update(loyalty_points: user_loyalty_points)
  end
end
