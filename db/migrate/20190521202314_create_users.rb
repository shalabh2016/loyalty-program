class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :username,  null: false, unique: true
      t.string :invited_by, null:false
      t.datetime :date_from_input,  null: false
      t.decimal :loyalty_points, :precision=>64, :scale=>12 , null: false, default: 0.00000
      t.timestamps
    end

    add_index :users, :username, unique: true
  end
end
