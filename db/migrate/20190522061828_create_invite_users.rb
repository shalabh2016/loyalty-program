class CreateInviteUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :invite_users do |t|
      t.integer :users_id
      t.integer :invites_id

      t.timestamps
    end
    add_index :invite_users, :users_id
    add_index :invite_users, :invites_id
  end
end
