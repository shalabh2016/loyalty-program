class CreateInvites < ActiveRecord::Migration[5.2]
  def change
    create_table :invites do |t|
      t.string :invited_to_username,  null: false
      t.integer :users_id, index: true
      t.string :invited_by, null: false
      t.datetime :date_from_input, null: false
      t.integer :accepted_or_not, null: false, default: 0
      
      t.timestamps
    end
    add_index :invites, :invited_to_username, unique: true
  end
end
