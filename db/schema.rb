# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_05_22_061828) do

  create_table "invite_users", force: :cascade do |t|
    t.integer "users_id"
    t.integer "invites_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["invites_id"], name: "index_invite_users_on_invites_id"
    t.index ["users_id"], name: "index_invite_users_on_users_id"
  end

  create_table "invites", force: :cascade do |t|
    t.string "invited_to_username", null: false
    t.integer "users_id"
    t.string "invited_by", null: false
    t.datetime "date_from_input", null: false
    t.integer "accepted_or_not", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["invited_to_username"], name: "index_invites_on_invited_to_username", unique: true
    t.index ["users_id"], name: "index_invites_on_users_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "username", null: false
    t.string "invited_by", null: false
    t.datetime "date_from_input", null: false
    t.decimal "loyalty_points", precision: 64, scale: 12, default: "0.0", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["username"], name: "index_users_on_username", unique: true
  end

end
