require 'test_helper'

class LoyaltyApiControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get loyalty_api_index_url
    assert_response :success
  end

  test "should get create" do
    get loyalty_api_create_url
    assert_response :success
  end

end
